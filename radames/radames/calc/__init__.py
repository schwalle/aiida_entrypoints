from one import OneCalculation
from two import TwoCalculation

__all__ = [OneCalculation, TwoCalculation]
