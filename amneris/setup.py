from setuptools import setup
from amneris.plugins import get_entry_points

setup(
    name='amneris',
    version='1.0a1',
    description='Minimal example for a plugin architecture for AiiDA',
    author='Rico Häuselmann',
    packages=['amneris'],
    classifiers=[
        'Development Status :: 3 - Alpha',
    ],
    entry_points=get_entry_points()
)
